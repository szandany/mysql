-- liquibase formatted sql
--changeset SteveZ:158821221
INSERT INTO `book` (`title`, `tags`)
VALUES (
  'ECMAScript 2015: A SitePoint Anthology',
  '["JavaScript", "ES2015", "JSON"]'
);
--rollback DELETE FROM book WHERE title='ECMAScript 2015: A SitePoint Anthology';