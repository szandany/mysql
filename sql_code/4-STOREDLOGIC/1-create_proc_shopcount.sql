-- liquibase formatted sql
--changeset BobR:5_456 endDelimiter:#
DROP PROCEDURE IF EXISTS get_shopcount;
#
CREATE PROCEDURE get_shopcount()
BEGIN
Select count(*) from shop1;
END
#
--rollback DROP PROCEDURE IF EXISTS get_shopcount;