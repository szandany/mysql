-- liquibase formatted sql
-- changeset SteveZ:1588217
CREATE INDEX idx_department
ON department (id, active);
--rollback DROP INDEX idx_department ON department;