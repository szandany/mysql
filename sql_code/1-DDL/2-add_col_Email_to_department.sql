-- liquibase formatted sql
-- changeset SteveZ:1588218
ALTER TABLE department
ADD Email varchar(255);
--rollback ALTER TABLE department DROP COLUMN Email;